public class Java_If_else {
    public static void main(String[]args){
        int x= 15;
        int y= 20;  
        int time =20;
        int age = 16;
        
        
    if (20>18){
        
        System.out.println("20 is greater than 18");//block of code to be executed if the condition is true
    }
       
    if(x<y){    
        System.out.println("x is smaller than y");
    }
    if (time<18){
        System.out.println("Good day.");
    }else if (time>15){
        System.out.println("Good morning");// block of code to be executed if the condition 1 is false
    } 
    
    else{          
    System.out.println("Good evening");//block of code to be executed if the condition is false
    }
    String result =(age<18)? "Good day.": "Good evening.";// shortcut to write if else commmand
    System.out.println(result); // shortcut to write if else commmand
    }
  }
